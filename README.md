# moncee

A Clojure library designed to control MongoDB.
Interfaces are alomost the same as another library named Mongoika (https://github.com/yuushimizu/Mongoika).

[![Clojars Project](https://img.shields.io/clojars/v/theorems/moncee.svg)](https://clojars.org/theorems/moncee)

## Motivation
I prefer the interfaces of the library Mongoika, therefore I started developing to work with Mongoika 3.6+. 

## Usage

Coming soon.

## License

Copyright © 2018 Tsutomu Miyahsita

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
