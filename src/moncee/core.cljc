(ns moncee.core
  (:gen-class)
  (:require [taoensso.timbre :as log]
            [clojure.walk :as w :refer [postwalk]]
            [clojure.string :as str]
            [silvur.datetime :refer (datetime date<)])
  #?(:clj (:require [clojure.core.memoize :as m])
     :cljs (:require ["mongodb" :refer (MongoClient Cursor ObjectID)]
                     [cljs.pprint :refer (pprint)]
                     [clojure.core.async
                      :refer (chan >! <! close! go go-loop promise-chan put! take!)]))  
  #?(:clj (:import [clojure.lang Named Keyword]
                   [com.mongodb.client
                    MongoClients MongoClient MongoIterable MongoCollection MongoDatabase MongoCursor]
                   [com.mongodb ReadConcern WriteConcern MongoClientSettings]
                   [com.mongodb.client.internal MongoCollectionImpl]
                   [com.mongodb.client.model Filters Sorts UpdateOptions Indexes IndexOptions]
                   [com.mongodb.client.result UpdateResult]
                   [java.util ArrayList]
                   [org.bson.conversions Bson]
                   [org.bson.types ObjectId]
                   [org.bson.codecs.configuration CodecRegistries CodecRegistry]
                   [org.bson BsonValue Document]
                   [java.time LocalDateTime Instant])))



(defonce ^:dynamic *mongo* nil)
(defonce ^:dynamic *db* nil)

(def mongo-ops {< "$lt" > "$gt" <= "$lte" >= "$gte" not= "$ne"})
(declare mongo+aggregate)
(declare mongo+query)

(defprotocol BsonOperation
  (bson-id [this])
  (bson<- ^Bson [this])
  (bson-> [this]))

(extend-protocol BsonOperation
  Boolean
  (bson-> [this]
    #?(:clj this
       :cljs (clj->js this)))
  (bson<- [this]
    #?(:clj this
       :cljs (clj->js this)))
  
  clojure.lang.IPersistentMap
  (bson<- [this]
    #?(:clj  (reduce (fn [^Document r [k v]]
                       (doto r (.append (name k) (bson<- v))))
                     (Document.)
                     (postwalk #(if (and (keyword? %) (not= % :_id)) (name %) %) this))
       :cljs (clj->js m)))
  
  clojure.lang.ISeq
  (bson<- [this]
    #?(:clj (vec (map bson<- this))
       :cljs (clj->js x)))
  (bson-> [vs]
    (map bson-> vs))

  org.bson.Document
  (bson-> [doc]
    (loop [m {} d doc]
      (if-not (seq d)
        m
        (recur (let [[k v] (first d)]
                 (assoc m (keyword k) (bson-> v))) (rest d)))))

    
  org.bson.types.ObjectId
  (bson-id [this] this)
  (bson-> [oid]
    oid)
  (bson<- [oid]
    oid)

  clojure.lang.Keyword
  (bson-id [kw]
    (name kw))

  ;; String
  java.lang.String
  (bson-id [this]
    (org.bson.types.ObjectId. this))
  (bson<- [this]
    #?(:clj this
       :cljs (clj->js x)))
  (bson-> [s]
    s)

  ;; Integer
  java.lang.Integer
  (bson<- [this]
    #?(:clj this
       :cljs (clj->js x)))
  (bson-> [i]
    i)

  ;; Long
  java.lang.Long
  (bson<- [this]
    #?(:clj this
       :cljs (clj->js x)))
  (bson-> [l]
    l)
  
  ;; Double
  java.lang.Double
  (bson<- [this]
    #?(:clj this
       :cljs (clj->js x)))
  (bson-> [d]
    d)

  ;; Vector
  clojure.lang.PersistentVector
  (bson<- [this]
    #?(:clj (mapv bson<- this)
       :cljs (clj->js x)))
  (bson-> [vs]
    vs)

  ;; Date
  java.util.Date
  (bson<- [this]
    #?(:clj this
       :cljs (clj->js x)))
  (bson-> [date]
    date)

  ;; Instant
  java.time.Instant
  (bson<- [this]
    #?(:clj (date< this)
       :cljs (clj->js x)))
  (bson-> [inst]
    inst)

  ;; LocalDateTime
  java.time.LocalDateTime
  (bson<- [this]
    #?(:clj (date< this)
       :cljs (clj->js x)))
  (bson-> [ldt]
    (date< ldt))
  
  
  java.util.ArrayList
  (bson-> [ary]
    (vec (seq ary)))
  
  nil
  (bson<- [this]
    #?(:clj this
       :cljs (clj->js x)))
  (bson-> [this]
    #?(:clj this
       :cljs (clj->js x))))



(defn bound-client []
  *mongo*)


(defprotocol MonceeOperations
  (-limit [this])
  (-skip [this])
  (-match [this])
  
  (-project [this])
  (-sort [this])
  (-transduce [this])
  (-group [this])
  
  (-insert [this docs])
  (-delete [this])
  (-update [this upsert? only-one? m]))



#?(:clj
   (deftype Context [^clojure.lang.PersistentVector operations
                     ^MongoCollection source]
     clojure.lang.ILookup
     (valAt [this ^Object key]
       (condp = key
         :operations operations
         :source source
         nil))
     (valAt [this ^Object key ^Object not-found]
       (condp = key
         :operations (.operations this)
         :source (.source this)
         nil))
     clojure.lang.Associative
     (^boolean containsKey [this ^Object key]
      (or (= key :operations) (= key :source)))
     (^clojure.lang.IMapEntry entryAt [this ^Object key]
      (condp = key
        :operations (.operations this)
        :source (.source this)
        nil))
     (^clojure.lang.Associative assoc [this ^Object key ^Object val]
      (condp = key
        :operations (Context. val (.source this))
        :source (Context. (.operations this) this)
        this))
     clojure.lang.Counted
     (count [this]
       (if (< 0 (-limit this) Long/MAX_VALUE)
         (count (-> this seq))
         (.countDocuments (.source this)  (bson<- (-match this)))))
     clojure.lang.Sequential
     (seq [this]
       (if (some #{:aggregation} (map :type (.operations this)))
         (mongo+aggregate this)
         (mongo+query this)))
     
     clojure.lang.ISeq
     (first [this]
       (log/trace (bson<- (-match this)))
       (-> (.source this)
           (.find (bson<- (-match this)))
           (.projection (bson<- (-project this)))
           (.sort (bson<- (-sort this)))
           (.limit (-limit this))
           (.skip (-skip this))
           (.first)
           (bson->)))
     (more [this]
       (update this :operations (fnil conj [])
               {:op :$skip
                :v 1}))
     (^clojure.lang.ISeq cons [this ^Object o ])
     MonceeOperations
     (-limit [this]
       (or (last (sort (map :v (filter #(= (:op %) :$limit) (.operations this))))) 0))
     (-skip [this]
       (apply max (cons 0  (map :v (filter #(= (:op %) :$skip) (.operations this))))))
     (-match [this]
       (let [z (->> (.operations this)
                    (filter #(= (:op %) :$match))
                    (mapcat :v))]
         (if (seq z)
           (assoc {} :$and z)
           {})))

     (-project [this]
       (->> (.operations this)
            (filter #(= (:op %) :$project))
            (map :v)
            (reduce conj {})))

     (-sort [this]
       (->> (.operations this)
            (filter #(= (:op %) :$sort))
            (map :v)
            (reduce conj {})))

     (-transduce [this]
       (->> (.operations this)
            (filter #(= (:op %) :transduce))
            (map :v)
            (reduce conj {})))

     (-group [this]
       (->> (.operations this)
            (filter #(= (:op %) :$group))
            (map :v)
            (reduce conj {})) )
     (-insert [this docs]
       (.insertMany ^MongoCollection (.source this) (map bson<- docs)))
     (-delete [this]
       (-> ^MongoCollection (.source this) (.deleteMany (bson<- (-match this))) (.getDeletedCount)))
     (-update [this upsert? only-one? m]
       (let [result  (if only-one?
                                    (.updateOne ^MongoCollection (.source this)
                                                (bson<- (-match this))
                                                (bson<- m)
                                                (doto (UpdateOptions.) (.upsert upsert?)))
                                    (.updateMany ^MongoCollection (.source this)
                                                 (bson<- (-match this))
                                                 (bson<- m)
                                                 (doto (UpdateOptions.) (.upsert upsert?))))]
         (cond
           (and only-one? upsert?) (-> result (.getUpsertedId))
           (and only-one? (not upsert?)) (-> result (.getModifiedCount))
           (and (not only-one?) upsert?) (let [modified (-> result (.getModifiedCount))]
                                           (if (= 0 modified) 1 modified))
           (and (not only-one?) (not upsert?)) (-> result (.getModifiedCount)) ))))
   
   :cljs
   (deftype Context [operations source]
     ICounted
     (-count [this]
       (if (< 0 (-limit this) js/Number.MAX_VALUE)
         (.then (-> this seq) count)
         (.countDocuments source (bson<- (-match this)))))
     IAssociative
     (-assoc [this key val]
       (condp = key
         :operations (Context. val (.-source this))
         :source (Context. (.-operations this) this)
         this))
     ISeqable
     (-seq [this]
       (if (some #(= :group (:op %)) operations)
         (let [docs (transduce (map (fn [{:keys [op v]}]
                                      (bson<- (hash-map ({ ;; Aggregate
                                                          :group :$group
                                                          ;; Normal/Aggregate
                                                          :filter :$match
                                                          :project :$project
                                                          :sort :$sort
                                                          :limit :$limit
                                                          :skip :$skip} op)
                                                        v))))
                               conj
                               []
                               (filter #(not (= (:op %) :transduce)) operations))]
           (-> source (.aggregate docs)))
         
         (-> source
             (.find (bson<- (-match this)))
             (.project (bson<- (-project this)))
             (.sort (bson<- (-sort this)))
             (.limit (-limit this))
             (.skip (-skip this))
             seq)))
     ;; (.on (.stream (.batchSize (restrict :test) 100)) "data"  (fn [x] (prn x)))
     ISeq
     (-first [this]
       (-> (-seq this)
           (.next)))
     MonceeOperations
     (-insert [this docs]
       (try
         (.insertMany (.-source this) (clj->js (map bson<- docs)))
         (catch js/Error e e)))
     (-match [this key]
       (->> (.-operations this)
            (filter #(= (:op %) key))
            (map :v)
            (reduce conj {})))
     (-limit [this]
       (or (last (sort (map :v (filter #(= (:op %) :limit) (.-operations this))))) 0))
     (-skip [this]
       (apply max (cons 0  (map :v (filter #(= (:op %) :skip) (.-operations this))))))))

#?(:cljs (extend-type Cursor
           ISeqable
           (-seq [this]
             (.toArray this))))


#?(:clj
   (defn mongo+aggregate [^Context ctx]
     (let [docs  (->> (.operations ctx)
                      (filter #(not (= (:op %) :transduce)))
                      (transduce (map (fn [{:keys [op v]}]
                                        (if (sequential? v)
                                          (bson<- {op {:$and v}})
                                          (bson<- {op v}))))
                                 conj
                                 []))]
       
       (-> ^MongoCollection (.source ctx)
           (.aggregate docs)
           (sequence)
           (bson->)))))

#?(:clj
   (defn mongo+query [^Context ctx]
     (let [iter  (-> ^MongoCollection (.source ctx)
                       (.find (bson<- (-match ctx)))
                       (.projection (bson<- (-project ctx)))
                       (.sort (bson<- (-sort ctx)))
                       (.limit (-limit ctx))
                       (.skip (-skip ctx)))]
         (if (.first iter)
           (->> (sequence (reduce comp
                                  (list* (map bson->) (vals (-transduce ctx)))) iter))
           nil))))


(defprotocol MongoFundamental
  (-mongo [x])
  (-database [client x]))

#?(:clj (extend-protocol MongoFundamental
          clojure.lang.PersistentArrayMap
          (-mongo [{:keys [hosts host port user password read-pref read-concern write-concern ssl]
                    :or {host "localhost" port 27017}}]
            (let [uri (str "mongodb://" (cond-> (str/join "," (or hosts [(str host ":" (or port 27017))]))
                                          (and user password) (->> (str user ":" (java.net.URLEncoder/encode password) "@"))
                                          (or read-pref read-concern write-concern ssl) (str "/?")
                                          read-pref (str "readPreference=" (name read-pref))
                                          read-concern (str "&" "readConcernLevel=" (name read-concern))
                                          write-concern (str "&" "w=" (name write-concern))))]
              (log/debug "Mongo URI:" uri)
              (MongoClients/create uri)))
          clojure.lang.Keyword
          (-mongo [host]
            (-mongo {:host (name host) :port 27017}))
          String
          (-mongo [uri-or-host]
            (let [cooked (re-find #"^(mongodb.*://)([^@]*)@?(.*)" uri-or-host )]
              (-> (cond->> cooked
                    (seq (last cooked)) (zipmap [:raw :protocol :user-info :host-port])
                    (empty? (last cooked)) (zipmap [:raw :protocol :host-port]))
                  (as-> m
                      (let [[user password] (str/split (or (:user-info m) "") #":")]
                        (conj m {:user user :password password})))
                  (as-> m
                      (let [[host port] (str/split (or (:host-port m) "") #":")]
                        (conj m {:host host :port port})))
                  (->> (reduce (fn [r [k v]]
                                 (if (seq v) (assoc r k v) r)) {}))
                  (select-keys [:host :port :user :password])
                  (-mongo))))
          (-database [host db-name]
            (-database (-mongo host) db-name))
          com.mongodb.client.MongoClient
          (-database [client db-name]
            (-> client (.getDatabase (name db-name)))))
   :cljs (extend-protocol MongoFundamental
           cljs.core/PersistentArrayMap
           (-mongo [{:keys [hosts host port user password name] :or {host "localhost" port 27017}}]
             (let [uri (str "mongodb://" (if hosts
                                           (str/join "," hosts)
                                           (cond-> ""
                                             (and user password) (str user ":" password "@")
                                             host (str host)
                                             port (str ":" port)
                                             name (str "/" name))))]
               (log/debug "Mongo URI:" uri)
               (MongoClient. uri)))
           string
           (-mongo [uri-or-host]
             (if (re-find #"^mongodb://" uri-or-host)
              (MongoClients/create uri-or-host)
              (MongoClients/create (str "mongodb://" uri-or-host))))
           (-database [host db-name]
             (-database (-mongo host) db-name))
           cljs.core/Keyword
           (-mongo [host]
             (-mongo {:host (name host) :port 27017}))
           (-database [host db-name]
             (-database (-mongo host) db-name))
           MongoClient
           (-database [client db-name]
             (doto client (.connect (fn [err]
                                      (set! *mongo* client)
                                      (set! *db* (.db *mongo* (name db-name)))
                                      (prn "Connected")))))))

(defn mongo
  ([]
   (-mongo "localhost"))
  ([& opts]
   (apply -mongo opts)))

(defn database
  ([]
   (-database "localhost" "test"))
  ([{:keys [name] :as m}]
   (-database (-mongo m) name))
  ([arg0 arg1]
   (-database arg0 arg1)))

(defn set-default-client! [client]
  #?(:clj (alter-var-root #'*mongo* (constantly client))
     :cljs (set! *mongo* (doto client (.connect)))))

(defmacro with-db-binding [db & body]
  `(binding [*db* ~db]
     ~@body))


(defn set-default-db!
  "Set default database."
  ([]
   (set-default-db! (mongo :localhost) :test))
  ([db-name]
   (if *mongo*
     (set-default-db! *mongo* db-name)
     (do
       (set-default-client! (mongo))
       (set-default-db! *mongo* db-name))))
  ([client db-name]
   (if (or (keyword? client) (string? client))
     (let [cli (mongo client)]
       (set-default-client! cli)
       #?(:clj (alter-var-root #'*db* (constantly (database cli db-name)))
          :cljs (set! *db* (database cli db-name))))
     #?(:clj (do (set-default-client! client)
                 (alter-var-root #'*db* (constantly (database client db-name))))
        :cljs (.connect client (fn [err]
                                 (if err
                                   (log/error err)
                                   (do (set! *db* (.db client (name db-name)))
                                       (set! *mongo* client)
                                       (log/info "Connected"))))))))
  ([host port db-name]
   (set-default-client! (mongo {:name db-name :host host :port port}))
   (set-default-db! db-name))
  ([host port user password db-name]
   (set-default-client! (mongo {:user user :password password :name db-name :host host :port port}))
   (set-default-db! db-name)))

(defn disconnect! 
  ([]
   (.close @*mongo*)
   (reset! *mongo* nil)
   (reset! *db* nil))
  ([cli]
   (.close cli)))

(defmacro switch!
  "Syntax sugar of set-default-db"
  [db-name]
  `(if *mongo* (set-default-db! *mongo* ~db-name) (ex-info "Client not configured" {})))

(defn collection [x]
  (if-not *db*
    (throw (ex-info "*db* is not configured" {}))
    #?(:clj (-> ^MongoDatabase *db* (.getCollection (name x)))
       :cljs (.collection *db* (name x)))))


(defn run-command
  "(run-command {:compact <collection-name>})"
  [cmd-map]
  (if *db*
    (.runCommand ^MongoDatabase *db* (bson<- cmd-map))
    (throw (ex-info "No database connection" {}))))


(defprotocol MongoRequestContextIdentify
  (requester ^Context [this] ))


#?(:clj (extend-protocol MongoRequestContextIdentify
          Named
          (requester [this] (Context. [] (collection this)))
          Keyword
          (requester [this] (Context. [] (collection (name this))))
          Context
          (requester [this] this))
   :cljs (extend-protocol MongoRequestContextIdentify
           cljs.core.Keyword
           (requester [this] (Context. [] (collection (name this))))
           string
           (requester [this] (Context. [] (collection this)))
           Context
           (requester [this] this)))


;; Utils







;; (defn bson-id ^org.bson.types.ObjectId [x]
;;   #?(:clj (if (string? x) (org.bson.types.ObjectId. ^String x) x)
;;      :cljs (ObjectID. x)))

#?(:cljs (defn p* [x]
           (.then (if (instance? js/Promise x)
                    x
                    (seq x))
                  (fn [z]
                    (pprint (js->clj z))))))

;; Base function to modify attributes
(defn batch-size [cursor v]
  #?(:clj cursor
     :cljs (.batchSize cursor v)))

;; DB

(defn bound-db
  "Return bound database"
  []
  *db*)

(defn drop-database!
  "Drop database"
  []
  (when-let [db ^MongoDatabase *db*] (-> db (.drop))))

(defn list-database
  "List databases"
  []
  (when-let [cli ^MongoClient *mongo*] (->> cli (.listDatabases) (map bson->))))

(defn list-collection-names
  "List collections"
  []
  (map keyword (.listCollectionNames ^MongoDatabase *db*)))


;; Index


(defn list-index [query-source]
  #?(:clj (->> (.listIndexes ^MongoCollection (:source (requester query-source)))
               seq
               (map bson->))))


(defn- compound-index [m]
  #?(:clj (Indexes/compoundIndex
           ^ArrayList (reduce (fn [^ArrayList r [^clojure.lang.Keyword k ^Long v]]
                                (let [k (name k)
                                      opt (doto (ArrayList.) (.add k))]
                                  (.add r (cond
                                            (number? v) (if (< v 0)
                                                          (Indexes/descending opt)
                                                          (Indexes/ascending  opt))
                                            (= v :hashed) (Indexes/hashed k)
                                            (= v :text) (Indexes/text k)))
                                  r))
                              (ArrayList.)
                              (dissoc m :unique?)))))

(defn create-index! [query-source & index-maps]
  #?(:clj (let [indexes (set (map :key (list-index query-source)))
                ctx (requester query-source)]
            (doseq [m index-maps]
              (when-not (indexes m)
                (let [{:keys [unique? background?]} m
                      opts (doto (IndexOptions.)
                             (.unique (true? unique?))
                             (.background (or (nil? background?) (true? background?))))]
                  (.createIndex ^MongoCollection (:source ctx)
                                ^Bson (compound-index m)
                                ^IndexOptions opts)))))))



(defn drop-index! [query-source & index-keys]
  #?(:clj (let [ctx (requester query-source)]
            (doseq [m index-keys]
              (.dropIndex ^MongoCollection (:source ctx) ^Bson (bson<- m))))))

(defmacro with-read-concern [concern & body]
  `(binding [*db* (.withReadConcern *db* (condp = ~concern
                                           :local ReadConcern/LOCAL
                                           :available ReadConcern/AVAILABLE
                                           :majority ReadConcern/MAJORITY
                                           :linearizable ReadConcern/LINEARIZABLE
                                           :snapshot ReadConcern/SNAPSHOT
                                           ReadConcern/DEFAULT))]
     ~@body))


(defmacro with-write-concern [concern & body]
  `(binding [*db* (.withWriteConcern *db* (condp = ~concern
                                            :acknowledged WriteConcern/ACKNOWLEDGED
                                            :journaled WriteConcern/JOURNALED
                                            :majority WriteConcern/MAJORITY
                                            :unacknoledged WriteConcern/UNACKNOWLEDGED
                                            :w1 WriteConcern/W1
                                            :w2 WriteConcern/W2
                                            :w3 WriteConcern/W3
                                            WriteConcern/MAJORITY))]
     ~@body))


#?(:cljs (defn await
           "works with JS promises
  returns [err res]
  where err - a catched exception (reject)
  res - the result of the promise work (resolve)
  (go
    (let [[err res] (<! (await my-promise))]
      (println res)))
  "
           [promise]
           (let [port (chan)]
             (-> promise
                 (.then (fn [res] (put! port [nil res]))
                        (fn [err] (put! port [err nil])))
                 (.catch (fn [err] (put! port [err nil]))))
             (<! port))))



;; Disable annoying log
(log/merge-config! {:ns-filter {:allow #{"*"} :deny #{"org.mongodb.driver.*"}}})

(defn query [request-context]
  (seq ^Context (requester request-context)))

(defn restrict [& params-and-query-source]
  (let [ctx (requester (last params-and-query-source))]
    (update ctx :operations (fnil conj [])
            {:op :$match
             :v (->> (partition 2 (butlast params-and-query-source))
                     (reduce (fn [result [k v]]
                               (conj result (if (= k :_id)
                                              {(bson-id k) v}
                                              {k (cond
                                                   (map? v) (clojure.set/rename-keys v mongo-ops)
                                                   (keyword? v) v
                                                   :else v)})))
                             [])
                     ;; (assoc {} :$and)
                     )})))


(defn project [& kvs-and-query-source]
  (let [query-source (last kvs-and-query-source)
        selects (last (butlast kvs-and-query-source))
        kvs (into {} (if (vector? selects)
                       (map vector selects (repeat true))
                       (map vec (partition 2 (butlast kvs-and-query-source)))))]
    (-> (requester query-source)
        (update :operations conj {:op :$project
                                  :v kvs} ))))


(defn order [k direction query-source]
  (-> (requester query-source)
      (update  :operations conj  {:op :$sort
                                  :v {k (condp = direction :asc 1 :desc -1)}})))

(defn skip [count query-source]
  (-> (requester query-source)
      (update  :operations conj {:op :$skip
                                 :v count})))

(defn limit [count query-source]
  (-> (requester query-source)
      (update  :operations conj {:op :$limit
                                 :v count})))

(defn fetch [query-source]
  (-> ^Context (requester query-source)
      ^Context (update  :operations (fnil conj []) {:op :$limit
                                           :v 1})
      (.seq)
      (first)))

(defn insert! [query-source & docs]
  (-> ^Context (requester query-source) (-insert docs)))

(defn delete!
  ([query-source]
   (-> ^Context (requester query-source) (-delete)))
  ([k v & kvs-and-query-source]
   (let [query-source (last kvs-and-query-source)
         kvs (partition 2 (butlast kvs-and-query-source))]
     (delete! (reduce (fn [r [k# v#]]
                        (restrict k# v# r))
                      (restrict k v (requester query-source))
                      kvs)))))

(defn- -update! [upsert? only-one? & ops-and-query-source]
  (let [query-source (last ops-and-query-source)
        ovs (into {} (map vec (partition 2 (butlast ops-and-query-source))))]
    (-> ^Context (requester query-source) (-update upsert? only-one? ovs))))

(defn upsert! [& ops-and-query-source]
  (apply -update! true false ops-and-query-source))

(defn update! [& ops-and-query-source]
  (apply -update! false false ops-and-query-source))

(defn update-1! [& ops-and-query-source]
  (apply -update! false true ops-and-query-source))


(defn +transducer [xf query-source]
  (update (requester query-source) :operations conj {:op :transduce
                                                     :v {(keyword (gensym)) xf}}))

(defn map-after [f query-source]
  (+transducer (map f) query-source))



;; Todo
;; (ensure-index! :trades [:id :desc ] :unique true)
(defn ensure-index! [])




;; Aggregate
;; Ex. (aggregate {:$match {:i "EURJPY"}} {:$group {:_id nil :z {:$sum :$h}}} :prices)




(defn aggregate-stage
  ([op query-source]
   (aggregate-stage op :x query-source))
  ([op v query-source]
   (let [ctx (requester query-source)]
     (update ctx :operations (fnil conj [])
             {:op op
              :type :aggregation
              :v v})))
  ([op k0 v0  & params-and-query-source]
   (let [ctx (requester (last params-and-query-source))]
     (update ctx :operations (fnil conj [])
             {:op op
              :type :aggregation
              :v (->> (butlast params-and-query-source)
                      (partition 2 )
                      (map vec)
                      (cons [k0 v0] )
                      (into {:_id nil} ))}))))

;; Grouping
(defn group* [& params-and-query-source]
  (apply aggregate-stage :$group params-and-query-source))


;; Count 
(defn count* [& params-and-query-source]
  (apply aggregate-stage :$count params-and-query-source))

;; Add field
(defn set* [& params-and-query-source]
  (apply aggregate-stage :$set params-and-query-source))

;; Unwind array on field
(defn unwind* [& params-and-query-source]
  (apply aggregate-stage :$unwind params-and-query-source))
