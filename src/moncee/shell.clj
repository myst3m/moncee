(ns moncee.shell
  (:gen-class)
  (:require [moncee.core :refer :all]
            [silvur.datetime :refer (datetime datetime*)])
  (:require [taoensso.timbre :as log])
  (:require [clojure.tools.cli :refer [parse-opts]]))

(def options-schema
  [["-h" "--host MongoDB" "MongoDB server"
    :default "localhost"]
   ["-d" "--database Database" "Database to be connected"]
   [nil "--help" "This help"]])

(defn help 
  ([]
   (dorun (map prn (map #(symbol (str %)) (filter #(re-find #"moncee" (str %)) (all-ns))))))
  ([ns-name]
   (let [nss (ns-publics (the-ns ns-name))
         length (apply max (map #(count (str %)) (keys nss)))]
     (dorun
      (map println
           (keep identity (map (fn [[k v]]
                                 (when-let [args (:arglists (meta v))]
                                   (format (str "* %-" (+ 4 length) "s" "%s")
                                           k
                                           (or (:doc (meta v)) "-"))))
                               nss))))))

  ([ns-name fn-name]
   (let [nss (ns-publics (the-ns ns-name))]
     (let [[k n] (first (filter #(= fn-name (first %)) nss))]
       (println "*" k)
       (println "  " (:arglists (meta n)))))))


(defn run [initial-ns & {:keys [init args] :or {init #()}}]
  (let [{:keys [options summary]} (parse-opts args options-schema)]
    
    (if (:help options)
      (println summary)
      (clojure.main/repl :init (fn []
                                 (log/set-level! :warn)
                                 (when-let [host (:host options)]
                                   (set-default-client! (mongo host)))
                                 (when-let [db (:database options)]
                                   (set-default-db! db))
                                 (in-ns (symbol initial-ns))
                                 (require '[moncee.core :refer :all])
                                 (require '[silvur.datetime :refer :all])
                                 (init)
                                 (println "\nboot options: " options "\n"))))))

(defn -main [& args]
  (run 'moncee.shell :args args))
