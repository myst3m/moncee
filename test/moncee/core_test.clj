(ns moncee.core-test
  (:require [moncee.util :refer [bson-> bson<-]])
  (:import [com.mongodb.client MongoClients MongoClient MongoIterable MongoCollection MongoDatabase]
           [com.mongodb.client.model Filters Sorts UpdateOptions Indexes ]
           [com.mongodb MongoClientSettings]
           [com.mongodb.diagnostics.logging JULLogger])
  (:import [org.bson.codecs.configuration CodecRegistries CodecRegistry])
  (:import [clojure.lang Named])
  (:import [moncee Context])
  (:require [silvur.datetime :refer [datetime* datetime]])
  (:require [clojure.test :refer :all]
            [moncee.core :refer :all]
            [moncee.database :refer :all]
            [moncee.document :refer :all]))

(def test-server {:host "localhost" :port 27017})

(deftest mongo-test
  (let [m (mongo)]
    (try
      (is (instance? com.mongodb.client.MongoClient m))
      (finally (.close m))))
  (let [m (mongo test-server)]
    (try
      (is (instance? com.mongodb.client.MongoClient m))
      (finally (.close m)))))

(deftest database-test
  (let [client (mongo)
        db (database client  :test)]
    (try
      (is (= "test" (.getName db)))
      (finally (.close client)))))

(deftest set-default-db-test
  (let [client (mongo)]
    (try
      (set-default-db! client :test)
      (is (instance? MongoDatabase (bound-db)))
      (is (instance? MongoClient (bound-client)))
      (is (= "test" (.getName (bound-db))))
      (finally (.close client)))))

(deftest insert-delete-test
  (let [client (mongo)
        doc {:a 0 :b "string" :c :keyword :d [1 2 3] :e {:item [1 2 3]}}]
    (try
      (set-default-db! client :test)
      (delete! :items)
      (insert! :items doc)
      (delete! (restrict :a 0 :items))
      (is (= 0 (count (query :items))))
      (finally (.close client)))))

(deftest query-test
  (let [client (mongo)
        docs [{:a 0 :b "string" :c :keyword :d [1 2 3] :e {:item-0 [1 2 3] :item-1 {:x 1 :y 2 :z 3}}}
              {:a 1 :b "string" :c :keyword :d [1 2 3] :e {:item [1 2 3]}}]]
    (try
      (set-default-db! client :test)
      (delete! :items)
      (apply insert! :items docs)
      (is (= (map #(dissoc % :_id) (query :items)) (map bson-> (map bson<- docs))))
      (is (= (map #(dissoc % :_id) (restrict :a 0 :items)) (filter #(= (:a %) 0) (map bson-> (map bson<- docs)))))
      (is (= (map #(dissoc % :_id) (restrict :d [1 2 3] :items)) (filter #(= (:d %) [1 2 3]) (map bson-> (map bson<- docs)))))
      (delete! :items)
      (finally (.close client)))))




