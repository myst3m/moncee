# Change Log
All notable changes to this project will be documented in this file.

## 0.2.0 - 2018-06-12
### Changes
- Fix a issue that keyword cannot be used in 'restrict' function as an value to search
- Ommit unnecessary println


### Added
- Added a function 'map-after'
- Added LICENSE

## [Unreleased]
### Added
- Add files

## 0.1.0 - 2018-06-11
### Initial Release
- Implement fundamental functions for Read/Write/Update/Delete


[Unreleased]: https://github.com/your-name/moncee/compare/0.1.1...HEAD
[0.1.1]: https://github.com/your-name/moncee/compare/0.1.0...0.1.1
